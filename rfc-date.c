/*
 * Decast -- Podcast subscription plugin for DeaDBeeF audio player.
 *
 * Copyright (c) 2015 Viktor Semykin <thesame.ml@gmail.com>
 *
 * Licensed under terms of Zlib license. See COPYING for details.
 *
 */

#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
static const char *weekdays[] = {
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
};

static const char *months[] = {
    "Jan",  "Feb" ,  "Mar",  "Apr" ,  "May",  "Jun" ,
    "Jul",  "Aug" ,  "Sep",  "Oct" ,  "Nov",  "Dec"
};

#include "tzs.c"

static int
match_string (const char *source, const char **strs, int n, char **end)
{
    int i;
    int res = -1;

    for (i = 0; i < n; ++i)
        if (!strncasecmp (source, strs[i], strlen (strs[i])))
        {
            res = i;
            *end = (char*)(source + strlen (strs[i]));
            break;
        }
    return res;
}

time_t
gmmktime (struct tm *tm)
{
    char *tz;
    time_t res;

    tz = getenv ("TZ");
    if (tz)
        tz = strdupa (tz);
    setenv ("TZ", "", 1);
    tzset();
    res = mktime (tm);
    if (tz)
        setenv ("TZ", tz, 1);
    else
        unsetenv ("TZ");
    tzset();
    return res;
}

time_t
parse_rfc822_date (const char *source)
{
    char wday[4];
    char mon[4];
    char timezone[7];
    struct tm tm;
    int ret;
    char *end;
    time_t res;
    long offset;

    ret = sscanf (source,
        "%3s, " "%d "        "%3s " "%d "        "%d:"        "%d:"        "%d "        "%6s",
        wday,   &tm.tm_mday, mon,   &tm.tm_year, &tm.tm_hour, &tm.tm_min,  &tm.tm_sec,  timezone);

    if (ret != 8)
        return -1;

    tm.tm_year -= 1900;

    tm.tm_wday = match_string (wday, weekdays, ARRAY_SIZE(weekdays), &end);
    if (tm.tm_wday < 0 || tm.tm_wday >= 7)
        return -1;

    tm.tm_mon = match_string (mon, months, ARRAY_SIZE(months), &end);
    if (tm.tm_mon < 0 || tm.tm_mon >= 12)
        return -1;

    offset = strtol (timezone, &end, 10);
    if (end != timezone)
        offset = (offset / 100)*60 + (offset % 100);
    else
    {
        int zone = match_string (timezone, tznames, ARRAY_SIZE(tznames), &end);
        if (zone != -1)
            offset = tzoffsets[zone];
    }
    res = gmmktime (&tm);
    res -= offset * 60;
    return res;
}

