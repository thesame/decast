PREFIX?=/usr/local
DESTDIR?=${PREFIX}/lib/deadbeef
GTKVER?=2

DEPS=libxml-2.0 libcurl gtk+-${GTKVER}.0
CFLAGS+=$(shell pkg-config --cflags ${DEPS}) -I${PREFIX}/include
LDFLAGS+=$(shell pkg-config --libs ${DEPS})
CFLAGS+=-fPIC -D_GNU_SOURCE -Wall -Wno-pointer-sign
SOURCES=decast.c infobox.c rfc-date.c
SONAME=decast_gtk${GTKVER}.so
OBJS=$(SOURCES:.c=_gtk${GTKVER}.o)

%_gtk${GTKVER}.o: %.c
	${CC} -c ${CFLAGS} $< -o $@

${SONAME}: ${OBJS}
	${CC} ${OBJS} -shared -o $@ ${LDFLAGS}

all: ${SONAME}

clean:
	rm -f ${OBJS} ${SONAME}

install: ${SONAME}
	install -D -t "${DESTDIR}" ${SONAME}
