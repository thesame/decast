/*
 * Decast -- Podcast subscription plugin for DeaDBeeF audio player.
 *
 * Copyright (c) 2015 Viktor Semykin <thesame.ml@gmail.com>
 *
 * Licensed under terms of Zlib license. See COPYING for details.
 *
 */

#pragma once

time_t parse_rfc822_date (const char *source);
