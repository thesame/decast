/*
 * List of Timezone abbreviations.
 *
 * Based on Wikipedia page
 * http://en.wikipedia.org/wiki/List_of_time_zone_abbreviations
 *
 * Distributed under terms of CC-BY-SA license.
 *
 */

const char *tznames[] = {"ACDT", "ACST", "ACT", "ADT", "AEDT", "AEST", "AFT",
"AKDT", "AKST", "AMST", "AMST", "AMT", "AMT", "ART", "AST", "AST", "AWDT",
"AWST", "AZOST", "AZT", "BDT", "BIOT", "BIT", "BOT", "BRT", "BST", "BST",
"BTT", "CAT", "CCT", "CDT", "CDT", "CEDT", "CEST", "CET", "CHADT", "CHAST",
"CHOT", "ChST", "CHUT", "CIST", "CIT", "CKT", "CLST", "CLT", "COST", "COT",
"CST", "CST", "CST", "CST", "CST", "CT", "CVT", "CWST", "CXT", "DAVT", "DDUT",
"DFT", "EASST", "EAST", "EAT", "ECT", "ECT", "EDT", "EEDT", "EEST", "EET",
"EGST", "EGT", "EIT", "EST", "EST", "FET", "FJT", "FKST", "FKST", "FKT", "FNT",
"GALT", "GAMT", "GET", "GFT", "GILT", "GIT", "GMT", "GST", "GST", "GYT",
"HADT", "HAEC", "HAST", "HKT", "HMT", "HOVT", "HST", "ICT", "IDT", "IOT",
"IRDT", "IRKT", "IRST", "IST", "IST", "IST", "JST", "KGT", "KOST", "KRAT",
"KST", "LHST", "LHST", "LINT", "MAGT", "MART", "MAWT", "MDT", "MET", "MEST",
"MHT", "MIST", "MIT", "MMT", "MSK", "MST", "MST", "MST", "MUT", "MVT", "MYT",
"NCT", "NDT", "NFT", "NPT", "NST", "NT", "NUT", "NZDT", "NZST", "OMST", "ORAT",
"PDT", "PET", "PETT", "PGT", "PHOT", "PKT", "PMDT", "PMST", "PONT", "PST",
"PST", "PYST", "PYT", "RET", "ROTT", "SAKT", "SAMT", "SAST", "SBT", "SCT",
"SGT", "SLST", "SRET", "SRT", "SST", "SST", "SYOT", "TAHT", "THA", "TFT",
"TJT", "TKT", "TLT", "TMT", "TOT", "TVT", "UCT", "ULAT", "USZ1", "UTC", "UYST",
"UYT", "UZT", "VET", "VLAT", "VOLT", "VOST", "VUT", "WAKT", "WAST", "WAT",
"WEDT", "WEST", "WET", "WIT", "WST", "YAKT", "YEKT"};

const int tzoffsets[] = {630, 570, 480, -180, 660, 600, 270, -480, -540, -180,
300, -240, 240, -180, 180, -240, 540, 480, -60, 240, 480, 360, -720, -240,
-180, 360, 60, 360, 120, 390, -300, -240, 120, 120, 60, 825, 765, 480, 600,
600, -480, 480, -600, -180, -240, -240, -300, -360, 480, 570, 630, -300, 480,
-60, 525, 420, 420, 600, 60, -300, -360, 180, -240, -300, -240, 180, 180, 120,
0, -60, 540, -300, 600, 180, 720, -180, -180, -240, -120, -360, -540, 240,
-180, 720, -540, 0, -120, 240, -240, -540, 120, -600, 480, 300, 420, -600, 420,
180, 180, 270, 480, 210, 330, 60, 120, 540, 360, 660, 420, 540, 630, 660, 840,
720, -570, 300, -360, 60, 120, 720, 660, -570, 390, 180, 480, -420, 390, 240,
300, 480, 660, -150, 690, 345, -210, -210, -660, 780, 720, 360, 300, -420,
-300, 720, 600, 780, 300, -120, -180, 660, -480, 480, -180, -240, 240, -180,
660, 240, 120, 660, 240, 480, 330, 660, -180, -660, 480, 180, -600, 420, 300,
300, 780, 540, 300, 780, 720, 0, 480, 120, 0, -120, -180, 300, -270, 600, 240,
360, 660, 720, 120, 60, 60, 60, 0, 420, 480, 540, 300};
