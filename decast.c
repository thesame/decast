/*
 * Decast -- Podcast subscription plugin for DeaDBeeF audio player.
 *
 * Copyright (c) 2015 Viktor Semykin <thesame.ml@gmail.com>
 *
 * Licensed under terms of Zlib license. See COPYING for details.
 *
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <gtk/gtk.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/HTMLparser.h>
#include <libxml/parserInternals.h>
#include <curl/curl.h>
#include <deadbeef/deadbeef.h>
#include <deadbeef/gtkui_api.h>

#include "infobox.h"
#include "rfc-date.h"

#define ITUNES_NS "http://www.itunes.com/DTDs/Podcast-1.0.dtd"

#define ALLOCA_SPRINTF(dest, ...) \
    sprintf((char*)(dest = alloca(snprintf(NULL, 0, __VA_ARGS__)+1)), __VA_ARGS__)

static const int intervals[] = {
    0, 60, 300, 900, 3600, 10800, 86400
};
#define DEFAULT_INTERVAL 2

#define DEFAULT_DATE_FORMAT "%c"

static DB_functions_t *deadbeef;
static ddb_gtkui_t *gtkui_plugin;

static size_t
curl_to_xml (char *ptr, size_t size, size_t nmemb, void *ctx)
{
    xmlParserCtxtPtr xml = ctx;
    int err = xmlParseChunk (xml, ptr, size*nmemb, 0);
    if (err)
        return 0;
    return size*nmemb;
}

static size_t
localize_time (const char *source, char *buf, size_t bufsize)
{
    struct tm tm;
    time_t gmts;
    char *format = alloca (bufsize);

    gmts = parse_rfc822_date (source);
    if (gmts == -1)
    {
        int t = strlen (source);
        if (t >= bufsize)
            return 0;
        strcpy (buf, source);
        return t;
    }

    deadbeef->conf_get_str ("decast.date_format", DEFAULT_DATE_FORMAT, format, bufsize);
    localtime_r (&gmts, &tm);
    return strftime (buf, bufsize, format, &tm);
}

static int
http_date (time_t t, char *buf, size_t buflen)
{
    struct tm tm;
    gmtime_r (&t, &tm);
    return strftime (buf, buflen, "%a, %d %b %Y %H:%M:%S %Z", &tm);
}

static ddb_playItem_t *
pl_get_next_unref (ddb_playItem_t *pl)
{
    ddb_playItem_t *next = deadbeef->pl_get_next (pl, PL_MAIN);
    deadbeef->pl_item_unref (pl);
    return next;
}

//thread-unsafe
static ddb_playItem_t *
plt_find_by_guid (ddb_playlist_t *plt, const char *guid)
{
    ddb_playItem_t *it;

    for (it = deadbeef->plt_get_first (plt, PL_MAIN); it; it = pl_get_next_unref (it))
    {
        const char *meta = deadbeef->pl_find_meta (it, ":DECAST::guid");
        if (meta && !strcmp (meta, guid))
            return it;
    }
    return NULL;
}

static int
parse_duration (const char *src)
{
    int h,m,s,ret;

    ret = sscanf (src, "%d:%d:%d", &h, &m, &s);
    if (ret == 3)
        return h*3600+m*60+s;
    if (ret == 2)
        return h*60+m;
    if (ret == 1)
        return h;
    return -1;
}

static char *
strtrimdup (char *src)
{
    char *head, *tail;
    for (head = src;
        *head == '\r' || *head == '\n' || *head == ' ';
        ++head);

    for (tail = src + strlen (src) - 1;
        *tail == '\r' || *tail == '\n' || *tail == ' ';
        --tail);

    return strndup (head, tail-head+1);
}

static char *
strip_html (const char *source)
{
    char *res = NULL;
    char *tmp;

    //not that optimal way to handle html entities:
    htmlDocPtr html = htmlReadMemory (source, strlen (source), "", NULL, 0);
    if (!html)
        return NULL;

    // html/body/p
    if (html->children && html->children->next && html->children->next->children)
    {
        tmp = xmlNodeGetContent (html->children->next->children);
        res = strtrimdup (tmp);
        xmlFree (tmp);
    }

    xmlFreeDoc (html);
    return res;
}

//thread-unsafe
static void
process_feed_item (ddb_playlist_t* plt, xmlNodePtr item)
{
    char *url = NULL;
    char *guid = NULL;
    ddb_playItem_t *pl;

    xmlNodePtr child;
    int summary_over = 0;

    for (child = item->children; child; child = child->next)
    {
        if (!strcasecmp (child->name, "enclosure"))
            url = xmlGetProp (child, "url");

        else if (!strcasecmp (child->name, "guid"))
            guid = xmlNodeGetContent (child);
    }

    if (!url)
    {
        if (guid) xmlFree (guid);
        return;
    }

    if (!guid) guid = url;

    pl = plt_find_by_guid (plt, guid);
    if (!pl)
    {
        pl = deadbeef->plt_insert_file2 (11, plt, NULL, url, NULL, NULL, NULL);
        deadbeef->pl_item_ref (pl);
    }

    deadbeef->pl_delete_all_meta (pl);

    deadbeef->pl_replace_meta (pl, ":DECAST::guid", guid); //just in case we falled back to url

    for (child = item->children; child; child = child->next)
    {
        if (child->type != XML_ELEMENT_NODE)
            continue;

        char *source = xmlNodeGetContent (child);
        char *value = strip_html (source);
        xmlFree (source);
        if (!value)
            continue;

        const char *fullname;
        if (child->ns)
            ALLOCA_SPRINTF (fullname, ":DECAST::%s:%s", child->ns->href, child->name);
        else
            ALLOCA_SPRINTF (fullname, ":DECAST::%s", child->name);

        deadbeef->pl_append_meta (pl, fullname, value);

        if (!strcasecmp (child->name, "author"))
            deadbeef->pl_replace_meta (pl, "artist", value);

        else if (!strcasecmp (child->name, "title"))
            deadbeef->pl_replace_meta (pl, "title", value);

        else if (!strcasecmp (child->name, "pubDate"))
        {
            char localized[1024];
            size_t ret = localize_time (value, localized, sizeof (localized));
            if (ret)
                deadbeef->pl_replace_meta (pl, "year", localized);
        }

        else if ((child->ns && !strcasecmp(child->ns->href, ITUNES_NS)) &&
                !strcasecmp (child->name, "summary"))
        {
            summary_over = 1; //we prefer <itunes:summary> over <description>
            deadbeef->pl_replace_meta (pl, "comment", value);
        }

        else if (!summary_over && !strcasecmp (child->name, "description"))
            deadbeef->pl_replace_meta (pl, "comment", value);
        
        else if (!strcasecmp (child->name, "duration"))
        {
            int seconds = parse_duration (value);
            if (seconds != -1)
                deadbeef->plt_set_item_duration (plt, pl, (float)seconds);
        }
        free (value);
    }

    deadbeef->pl_item_unref (pl);

    xmlFree (url);
    if (guid != url)
        xmlFree (guid);
}

static void
process_feed (ddb_playlist_t *plt, xmlDocPtr doc)
{
    int i;
    xmlXPathContextPtr xpath;
    xmlXPathObjectPtr obj;

    deadbeef->pl_lock();
    xpath = xmlXPathNewContext (doc);
    obj = xmlXPathEval ("string(/rss/channel/title)", xpath);
    if (obj)
    {
        if (obj->type == XPATH_STRING)
            deadbeef->plt_set_title (plt, obj->stringval);
        xmlXPathFreeObject (obj);
    }
    else
        fprintf (stderr, "RSS error 1\n");

    xmlXPathFreeContext (xpath);

    xpath = xmlXPathNewContext (doc);
    obj = xmlXPathEval ("/rss/channel/item", xpath);
    if (obj)
    {
        deadbeef->plt_add_files_begin(plt, 11);
        for (i = 0; i < obj->nodesetval->nodeNr; ++i)
        {
            xmlNodePtr item = obj->nodesetval->nodeTab[i];
            process_feed_item (plt, item);
        }
        deadbeef->plt_add_files_end (plt, 11);
        xmlXPathFreeObject (obj);
    }
    else
        fprintf (stderr, "RSS error 2\n");

    xmlXPathFreeContext (xpath);
    deadbeef->pl_unlock();
}

static int
hdrfunc (char *buffer, size_t size, size_t nitems, void *ctx)
{
    char **lastmod = ctx;

    size *= nitems;
    if (!strncasecmp (buffer, "last-modified:", strlen ("last-modified:")))
    {
        if (*lastmod)
            free (*lastmod); //probably we already met "Date:" header
        *lastmod = strtrimdup (buffer + strlen ("last-modified:"));
    }
    if (!*lastmod && // only if "Last-Modified:" not met
        !strncasecmp (buffer, "date:", strlen ("date:")))
    {
        *lastmod = strtrimdup (buffer + strlen ("date:"));
    }
    return size;
}

/**
 * GETs XML document by URL
 * @param since "If-Modified-Since:" header in request
 * @param lastmod returns "Last-Modified:" or "Date:" header in response, if found. "Last-Modified:" is preferred.
 * @returns xmlDoc of parsed XML, or NULL if parsing failed
 */
static xmlDocPtr
get_xml (const char *url, const char *since, char **lastmod)
{
    CURL *curl;
    xmlParserCtxtPtr xml;
    struct curl_slist *hdrs = NULL;
    long http_code;
    xmlDocPtr doc;

    if (since)
    {
        char *header;
        ALLOCA_SPRINTF (header, "If-Modified-Since: %s", since);
        hdrs = curl_slist_append (hdrs, header);
    }

    *lastmod = NULL;

    xml = xmlCreatePushParserCtxt (NULL, NULL, NULL, 0, NULL);

    curl = curl_easy_init();
    curl_easy_setopt (curl, CURLOPT_URL, url);
    curl_easy_setopt (curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt (curl, CURLOPT_WRITEFUNCTION, curl_to_xml);
    curl_easy_setopt (curl, CURLOPT_WRITEDATA, xml);
    curl_easy_setopt (curl, CURLOPT_HEADERFUNCTION, hdrfunc);
    curl_easy_setopt (curl, CURLOPT_HEADERDATA, lastmod);
    curl_easy_setopt (curl, CURLOPT_HTTPHEADER, hdrs);
    CURLcode curl_code = curl_easy_perform (curl);
    curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
    curl_easy_cleanup(curl);

    //xmlParseChunk (xml, NULL, 0, 1);

    doc = xml->myDoc;
    xmlFreeParserCtxt (xml);

    //printf ("curl %d; http %ld\n", curl_code, http_code);
    if (!(http_code == 200 && curl_code == CURLE_OK))
    {
        xmlFreeDoc (doc);
        doc = NULL;
    }
    return doc;
}

static void
fetch_thread (void *ctx)
{
    ddb_playlist_t *plt = ctx;
    xmlDocPtr doc;
    const char *since;
    char *lastmod;
    const char *url;

    url = deadbeef->plt_find_meta (plt, "decast-url");
    since = deadbeef->plt_find_meta (plt, "decast-lastmod");

    doc = get_xml (url, since, &lastmod);
    if (doc)
    {
        if (!lastmod)
            http_date (time(NULL), lastmod = malloc(64), 64);

        deadbeef->plt_replace_meta (plt, "decast-lastmod", lastmod);
        process_feed (plt, doc);
        xmlFreeDoc (doc);
    }
    if (lastmod)
        free (lastmod);
}

static void
update_plt (ddb_playlist_t *plt)
{
    const char *url = deadbeef->plt_find_meta (plt, "decast-url");
    if (!url)
        return;
    deadbeef->thread_start (fetch_thread, plt);
}

static void
update_all ()
{
    int i, count;

    deadbeef->pl_lock();
    count = deadbeef->plt_get_count();
    for (i = 0; i < count; ++i)
    {
        ddb_playlist_t *playlist = deadbeef->plt_get_for_idx (i);
        update_plt (playlist);
    }
    deadbeef->pl_unlock();
}

static void
add_decast_plt (const char *url)
{
    deadbeef->pl_lock();
    int idx = deadbeef->plt_add (deadbeef->plt_get_count(), url);
    ddb_playlist_t *plt = deadbeef->plt_get_for_idx (idx);
    deadbeef->plt_add_meta (plt, "decast-url", url);
    deadbeef->plt_unref (plt);
    deadbeef->pl_unlock();

    update_all();
}

static int
open_url_cb (struct DB_plugin_action_s *action, int ctx)
{
    decast_subscribe_dialog (gtkui_plugin, &add_decast_plt);
    return 0;
}

static int
update_url_cb (struct DB_plugin_action_s *action, int ctx)
{
    update_all();
    return 0;
}

static int
eps_info_cb (struct DB_plugin_action_s *action, int ctx)
{
    if (ctx == DDB_ACTION_CTX_SELECTION)
    {
        deadbeef->pl_lock();
        ddb_playlist_t *plt = deadbeef->plt_get_curr ();
        ddb_playItem_t *it;

        for (it = deadbeef->plt_get_first (plt, PL_MAIN); it; it = pl_get_next_unref (it))
        {
            if (deadbeef->pl_is_selected (it))
            {
                show_infobox (deadbeef, it, gtkui_plugin);
                deadbeef->pl_item_unref (it);
                break;
            }
        }

        deadbeef->plt_unref (plt);
        deadbeef->pl_unlock();
    }
    return 0;
}

static DB_plugin_action_t action_update_url = {
    .title = "Podcasts/Update Podcasts",
    .name = "decast_update_url",
    .flags = DB_ACTION_ADD_MENU | DB_ACTION_COMMON,
    .callback2 = &update_url_cb,
    .next = NULL
};

static DB_plugin_action_t action_open_url = {
    .title = "Podcasts/Subscribe to Podcast...",
    .name = "decast_open_url",
    .flags = DB_ACTION_ADD_MENU | DB_ACTION_COMMON,
    .callback2 = &open_url_cb,
    .next = &action_update_url
};

static DB_plugin_action_t action_eps_info = {
    .title = "Episode Info...",
    .name = "decast_track",
    .flags = DB_ACTION_ADD_MENU | DB_ACTION_SINGLE_TRACK,
    .callback2 = &eps_info_cb,
    .next = NULL
};

static DB_plugin_action_t*
get_actions (DB_playItem_t *it)
{
    if (it)
    {
        deadbeef->pl_lock();
        const char *guid = deadbeef->pl_find_meta (it, ":DECAST::guid");
        deadbeef->pl_unlock();
        if (!guid)
            return NULL;
        return &action_eps_info;
    }
    return &action_open_url;
}

static int
plugin_connect ()
{
    gtkui_plugin = (ddb_gtkui_t *)deadbeef->plug_get_for_id (DDB_GTKUI_PLUGIN_ID);
    if (!gtkui_plugin) {
        fprintf (stderr, "decast: can't find "DDB_GTKUI_PLUGIN_ID" plugin\n");
        return -1;
    }
    return 0;
}

static gboolean
timeout_cb (void *ctx)
{
    update_all();
    return TRUE;
}

static void
start_timer ()
{
    int opt;
    static int prev_opt = -1;
    static guint timer_id = 0;

    opt = deadbeef->conf_get_int ("decast.timeout", DEFAULT_INTERVAL);
    if (opt < 0 || opt >= (sizeof(intervals)/sizeof(intervals[0])))
        opt = DEFAULT_INTERVAL;

    if (opt != prev_opt)
    {
        prev_opt = opt;
        if (timer_id)
            g_source_remove (timer_id);

        if (intervals[opt])
            timer_id = g_timeout_add_seconds (intervals[opt], &timeout_cb, NULL);
        else
            timer_id = 0;
    }
}

static int
recv_message (uint32_t id, uintptr_t ctx, uint32_t p1, uint32_t p2)
{
    if (id == DB_EV_PLUGINSLOADED || id == DB_EV_CONFIGCHANGED)
        start_timer();
    return 0;
}

static const char config_dlg[] =
    "property \"Update interval\" select[7] decast.timeout 2 \"Never\" \"1 min\" \"5 min\" \"15 min\" \"1 hour\" \"3 hours\" \"24 hours\";"
    "property \"Date Format\" entry decast.date_format \""DEFAULT_DATE_FORMAT"\";"
    ;

static DB_misc_t decast_plugin = {
    DDB_REQUIRE_API_VERSION(1,0)

#if GTK_CHECK_VERSION(3,0,0)
    .plugin.id = "decast_gtk3",
#else
    .plugin.id = "decast_gtk2",
#endif
    .plugin.version_major = 0,
    .plugin.version_minor = 1,
    .plugin.name = "Podcast support",
    .plugin.descr = "Allows to subscribe and receive Podcasts",
    .plugin.copyright =
        "Copyright (c) 2015 Viktor Semykin <thesame.ml@gmail.com>\n"
        "\n"
        "This software is provided 'as-is', without any express or implied\n"
        "warranty. In no event will the authors be held liable for any damages\n"
        "arising from the use of this software.\n"
        "\n"
        "Permission is granted to anyone to use this software for any purpose,\n"
        "including commercial applications, and to alter it and redistribute it\n"
        "freely, subject to the following restrictions:\n"
        "\n"
        "1. The origin of this software must not be misrepresented; you must not\n"
        "   claim that you wrote the original software. If you use this software\n"
        "   in a product, an acknowledgement in the product documentation would be\n"
        "   appreciated but is not required.\n"
        "2. Altered source versions must be plainly marked as such, and must not be\n"
        "   misrepresented as being the original software.\n"
        "3. This notice may not be removed or altered from any source distribution.\n",
    .plugin.website = "http://bitbucket.org/thesame/decast",
    .plugin.type = DB_PLUGIN_MISC,
    .plugin.get_actions = &get_actions,
    .plugin.connect = &plugin_connect,
    .plugin.configdialog = config_dlg,
    .plugin.message = recv_message
};

DB_plugin_t *
#if GTK_CHECK_VERSION(3,0,0)
decast_gtk3_load
#else
decast_gtk2_load
#endif
(DB_functions_t *api)
{
    deadbeef = api;
    return &decast_plugin.plugin;
}
