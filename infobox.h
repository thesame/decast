/*
 * Decast -- Podcast subscription plugin for DeaDBeeF audio player.
 *
 * Copyright (c) 2015 Viktor Semykin <thesame.ml@gmail.com>
 *
 * Licensed under terms of Zlib license. See COPYING for details.
 *
 */

#pragma once

void show_infobox (DB_functions_t *deadbeef, ddb_playItem_t *pl, ddb_gtkui_t *gtkui_plugin);
void decast_subscribe_dialog (ddb_gtkui_t *gtkui_plugin, void (*callback)(const char *url));
