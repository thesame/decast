/*
 * Decast -- Podcast subscription plugin for DeaDBeeF audio player.
 *
 * Copyright (c) 2015 Viktor Semykin <thesame.ml@gmail.com>
 *
 * Licensed under terms of Zlib license. See COPYING for details.
 *
 */

#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <deadbeef/deadbeef.h>
#include <deadbeef/gtkui_api.h>

#define ITUNES_NS "http://www.itunes.com/DTDs/Podcast-1.0.dtd"

//NULL-safe version of strdupa:
#define STRDUPA(x) \
    ((__tmp = x) ? strdupa (__tmp) : NULL)


/********************* Episode info dialog *********************************/

//A little trick to make label keep viewport width
static void
child_resize (GtkWidget *widget, GdkRectangle *allocation, void *ctx)
{
    GtkWidget *child = GTK_WIDGET(ctx);

    gtk_widget_set_size_request (child, allocation->width, -1);
}

void
show_infobox (DB_functions_t *deadbeef, ddb_playItem_t *pl, ddb_gtkui_t *gtkui_plugin)
{
    //hope someday I can do it with run_dialog
    const char *__tmp;

    deadbeef->pl_lock();
    const char *title = STRDUPA(deadbeef->pl_find_meta (pl, "title"));
    const char *url = STRDUPA(deadbeef->pl_find_meta (pl, ":DECAST::link"));
    const char *date = STRDUPA(deadbeef->pl_find_meta (pl, "year"));
    const char *summary = STRDUPA(deadbeef->pl_find_meta (pl, ":DECAST::"ITUNES_NS":summary"));

    const char *author = STRDUPA(deadbeef->pl_find_meta (pl, ":DECAST::"ITUNES_NS":author"));
    if (!author) author = STRDUPA(deadbeef->pl_find_meta (pl, ":DECAST::author"));
    if (!author) author = STRDUPA(deadbeef->pl_find_meta (pl, "artist"));
    if (!author) author = "unknown";
    deadbeef->pl_unlock();

    GtkWidget *dlg = gtk_dialog_new_with_buttons (
        "Episode Info",
        GTK_WINDOW (gtkui_plugin->get_mainwin()),
        GTK_DIALOG_MODAL,
        "_OK", GTK_RESPONSE_ACCEPT,
        NULL
    );
    gtk_window_set_default_size (GTK_WINDOW (dlg), 400, 400);

    GtkWidget *content_area = gtk_dialog_get_content_area (GTK_DIALOG (dlg));

    GtkWidget *scrl = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrl), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start (GTK_BOX (content_area), scrl, TRUE, TRUE, 0);

    GtkWidget *label = gtk_label_new (NULL);
    gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);

    GtkWidget *viewport = gtk_viewport_new (NULL, NULL);
    gtk_viewport_set_shadow_type (GTK_VIEWPORT(viewport), GTK_SHADOW_NONE);

    gtk_container_add (GTK_CONTAINER (viewport), label);
    gtk_container_add (GTK_CONTAINER (scrl), viewport);
    g_signal_connect (viewport, "size-allocate", G_CALLBACK(child_resize), label);

    char *markup;
    markup = g_markup_printf_escaped ("<a href=\"%s\">%s</a> by <b>%s</b>\nat <i>%s</i>\n\n%s", url, title, author, date, summary);
    gtk_label_set_markup (GTK_LABEL(label), markup);
    g_free (markup);

    g_signal_connect (dlg, "response", G_CALLBACK (gtk_widget_destroy), dlg);
    gtk_widget_show_all (dlg);
}

/************************* Subscription dialog ********************************/
static char *add_url;
static void (*dlgsubscr_cb)(const char *url);

static void
set_param (const char *key, const char *value)
{
    if (add_url)
        free (add_url);
    add_url = strdup (value);
}

static void
get_param (const char *key, char *value, int len, const char *def)
{
    *value = 0;
}

static int
dlgsubscr_mid_cb (int button, void *ctx)
{
    if (add_url)
    {
        dlgsubscr_cb (add_url);
        free (add_url);
        add_url = NULL;
    }
    return 0;
}

void
decast_subscribe_dialog (ddb_gtkui_t *gtkui_plugin, void (*callback)(const char *url))
{
    static ddb_dialog_t dlg = {
        .title = "Subscribe to Podcast",
        .layout = "property \"Podcast URL\" entry url \"\";",
        .set_param = &set_param,
        .get_param = &get_param
    };

    dlgsubscr_cb = callback;

    gtkui_plugin->gui.run_dialog (&dlg, 1<<ddb_button_ok, &dlgsubscr_mid_cb, NULL);
}
